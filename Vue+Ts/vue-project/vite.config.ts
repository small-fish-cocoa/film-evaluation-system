import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },

  server:{
    open:true,   //默认启动项目打开页面
    port:5173,   //端口号
    host:"0.0.0.0",
    proxy:{
      '/api': {	//
        target: "http://localhost:8080", // 目标地址
        ws: true,
        secure: false,
        changeOrigin: true,// 是否允许跨域代理
        rewrite: (path) => path.replace(/^\/api/, '') // 重定向地址
      }
    }
  },

})
