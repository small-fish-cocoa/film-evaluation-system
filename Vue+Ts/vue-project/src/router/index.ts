 import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: '/',
      name: 'a',
      component: () => import('../components/a.vue')
    },
    {
      path: '/pn',
      name: 'pn',
      component: () => import('../components/PNView.vue')
    }, {
      path: '/home',
      name: 'home',
      component: () => import('../components/HomeView.vue')
    }, {
      path: '/admin',
      name: 'admin',
      component: () => import('../components/AdminView.vue')
    }, {
      path: '/login',
      name: 'login',
      component: () => import('../components/LoginView.vue')
    },
    {
      path: '/user',
      name: 'user',
      component: () => import('../components/UserView.vue')
    }
    // }, {
    //   path: '/password',
    //   name: 'password',
    //   component: () => import('../components/PasswordView.vue')
    // }
  ]
})

export default router
