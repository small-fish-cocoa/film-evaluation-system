create table user
(
    email    varchar(20) primary key                     not null,
    password varchar(50)                                 not null,
    gender   varchar(10) CHECK ( gender in ('男', '女')) not null,
    user     varchar(20)                                 not null,
#     电话
    phone    varchar(11),
#     城市
    district varchar(40),
    img      varchar(255)
) engine = InnoDB
  default charset = utf8;

create table Film
(
    id       int primary key auto_increment not null,
#     用户名
    email    varchar(20)                    not null,
#     电影名称
    name     varchar(255)                   not null,
# 导演
    director varchar(255)                   not null,
    #     主演
    zhu      varchar(255)                   not null,
#     上映时间
    date     varchar(255)                   not null,
#     电影播放时间
    time     varchar(255)                   not null,
#     类别
    lei      varchar(255)                   not null,
#     赞
    zhang    long                           not null,
#     差评
    chai     long                           not null,
#     海报地址
    url      varchar(255)
) engine = InnoDB
  default charset = utf8;


create table pn
(
    id    int primary key auto_increment not null,
    film  varchar(255)                   not null,
    time  varchar(255),
    email varchar(50),
    user  varchar(200),
    textd text,
    zhan  int,
    chai  int
) engine = InnoDB
  default charset = utf8;

create table dysc
(
    id    int auto_increment primary key,
    email varchar(30)  not null,
    name  varchar(255) not null,
    url   varchar(255) not null,
    zhan  int
) engine = InnoDB
  default charset = utf8;

create table jb
(
    id     int auto_increment primary key not null,
    emaila varchar(20)                    not null,
    emailn varchar(20)                    not null,
    data   text                           not null
) engine InnoDB
  default charset = utf8;

alter table film
    add constraint film_user_email_fk
        foreign key (email) references user (email);