package com.example.demo.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.demo.pojo.Dysc;
import com.example.demo.pojo.User;
import com.example.demo.service.DyscService;
import com.example.demo.service.TokenService;
import com.example.demo.service.UserService;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@RestController("user")
@RequestMapping("user")
public class UserController {

    @Resource
    private UserService userService;

    @Resource
    private TokenService tokenService;

    @Resource
    private DyscService dyscService;

    @GetMapping("remUser")
    public boolean remUser(@RequestParam("id") String id, @RequestHeader("Authorization") String token) {
        Map<String, String> parseToken = this.tokenService.parseToken(token);
        String email = parseToken.get("email");
        String hxPassword = parseToken.get("hxPassword");
        List<User> userList = this.userService.getUser(email);
        if (Objects.equals("admin", email) && !userList.isEmpty() && Objects.equals(hxPassword, userList.get(0).getPassword())) {
            QueryWrapper<User> dyscQueryWrapper = new QueryWrapper<>();
            dyscQueryWrapper.eq("email", id);
            this.userService.remove(dyscQueryWrapper);
            return true;
        }
        return false;
    }

    @GetMapping("getName")
    private Map<String, Object> getName(@RequestHeader("Authorization") String token) {
        Map<String, Object> map = new HashMap<>();
        Map<String, String> parsedToken = this.tokenService.parseToken(token);
        String email = parsedToken.get("email");
        String hxPassword = parsedToken.get("hxPassword");
        List<User> userList = this.userService.getUser(email);
        if (!userList.isEmpty() && Objects.equals(userList.get(0).getPassword(), hxPassword)) {
            map.put("user", userList.get(0).getUser());
            map.put("img", userList.get(0).getImg());
            map.put("email", email);
            map.put("msg", true);
            map.put("code", 200);
            return map;
        }
        map.put("msg", false);
        map.put("code", 401);
        return map;
    }

    @GetMapping("getUser")
    public Map<String, Object> getUser(
            @RequestHeader("Authorization") String token) {
        Map<String, String> tokenMap = this.tokenService.parseToken(token);
        String tokenEmail = tokenMap.get("email");
        String tokenPassword = tokenMap.get("hxPassword");
        List<User> userList = userService.getUser(tokenEmail);
        Map<String, Object> map = new HashMap<>();
        if (!userList.isEmpty()) {
            String password = userList.get(0).getPassword();
            String email = userList.get(0).getEmail();
            if (Objects.equals(tokenEmail, email) && Objects.equals(tokenPassword, password)) {
                map.put("email", email);
                map.put("user", userList.get(0).getUser());
                map.put("gender", userList.get(0).getGender());
                map.put("phone", userList.get(0).getPhone());
                map.put("district", userList.get(0).getDistrict());
                map.put("img", userList.get(0).getImg());
                map.put("msg", true);
                map.put("code", 200);
                return map;
            }

        }
        map.put("hint", "token无效！");
        map.put("msg", false);
        map.put("code", 500);
        return map;
    }

    @PostMapping("updateUser")
    public Map<String, Object> updateUser(@RequestParam("user") String user,
                                          @RequestParam("gender") String gender,
                                          @RequestParam("phone") String phone,
                                          @RequestParam("district") String district,
                                          @RequestHeader("Authorization") String token) {
        Map<String, String> tokenMap = this.tokenService.parseToken(token);
        String tokenEmail = tokenMap.get("email");
        String tokenPassword = tokenMap.get("hxPassword");
        List<User> userList = this.userService.getUser(tokenEmail);
        Map<String, Object> map = new HashMap<>();
        if (!userList.isEmpty()) {
            String password = userList.get(0).getPassword();
            String email = userList.get(0).getEmail();
            if (Objects.equals(tokenEmail, email) && Objects.equals(tokenPassword, password)) {
                this.userService.updateUser(email, user, gender, phone, district);
                map.put("hint", "修改成功！");
                map.put("msg", true);
                map.put("code", 200);
            } else {
                map.put("hint", "token无效！");
                map.put("msg", false);
                map.put("code", 500);
            }
            return map;
        } else {
            map.put("hint", "token无效！");
            map.put("msg", false);
            map.put("code", 500);
        }
        return map;

    }

    @GetMapping("getUserMap")
    public Map<String, Object> get(
            @RequestParam("k") int k,
            @RequestHeader("Authorization") String token) {

        Map<String, String> parseToken = this.tokenService.parseToken(token);
        Map<String, Object> map = new HashMap<>();
        String email = parseToken.get("email");
        String hxPassword = parseToken.get("hxPassword");
        List<User> userList = this.userService.getUser(email);

        if (!userList.isEmpty() && Objects.equals(userList.get(0).getPassword(), hxPassword) && Objects.equals(email, "admin")) {
            List<User> allUser = this.userService.getAllUser(k, k + 29);
            map.put("data", allUser);
            map.put("msg", true);
            map.put("code", 200);
            return map;
        }
        map.put("hint", "无效token！");
        map.put("msg", false);
        map.put("code", 500);
        return map;
    }

    @GetMapping("getGz")
    public Map<String, Object> getGz(@RequestHeader("Authorization") String token) {
        Map<String, String> tokenMap = this.tokenService.parseToken(token);
        Map<String, Object> map = new HashMap<>();
        String email = tokenMap.get("email");
        String hxPassword = tokenMap.get("hxPassword");
        List<User> userList = this.userService.getUser(email);
        if (!userList.isEmpty() && Objects.equals(userList.get(0).getPassword(), hxPassword)) {
            QueryWrapper<Dysc> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("email", email);
            List<Dysc> list = this.dyscService.list(queryWrapper);
            map.put("data", list);
            map.put("msg", true);
            map.put("code", 200);
            return map;
        }
        map.put("msg", false);
        map.put("code", 500);
        return map;
    }
}
