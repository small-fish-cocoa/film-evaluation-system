package com.example.demo.pojo;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serial;
import java.io.Serializable;
import lombok.Data;

/**
 * @TableName film
 */
@TableName(value ="film")
@Data
public class Film implements Serializable {
    private int id;

    private String email;

    private String name;

    private String director;

    private String zhu;

    private String date;

    private String time;

    private String lei;

    private String zhang;

    private String chai;

    private String url;

    @Serial
    private static final long serialVersionUID = 1L;
}