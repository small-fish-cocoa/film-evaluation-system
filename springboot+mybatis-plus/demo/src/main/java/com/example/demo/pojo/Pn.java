package com.example.demo.pojo;

import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serial;
import java.io.Serializable;

import lombok.Data;

/**
 * @TableName pn
 */
@TableName(value = "pn")
@Data
public class Pn implements Serializable {
    private Integer id;
    private String film;
    private String time;
    private String email;
    private String user;
    private String url;
    private String jb;
    private String textd;
    private Integer zhan;
    private Integer chai;
    @Serial
    private static final long serialVersionUID = 1L;
}