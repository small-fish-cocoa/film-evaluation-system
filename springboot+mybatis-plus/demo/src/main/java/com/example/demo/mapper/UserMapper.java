package com.example.demo.mapper;

import com.example.demo.pojo.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* @author 13465
* @description 针对表【user】的数据库操作Mapper
* @createDate 2024-06-17 19:07:37
* @Entity com.example.demo.pojo.User
*/

@Mapper
public interface UserMapper extends BaseMapper<User> {

}




