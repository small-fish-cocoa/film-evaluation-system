package com.example.demo.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.example.demo.common.FilmCommon;
import com.example.demo.common.PfCommon;
import com.example.demo.pojo.Film;
import com.example.demo.pojo.Pf;
import com.example.demo.pojo.User;
import com.example.demo.service.FilmService;
import com.example.demo.service.TokenService;
import com.example.demo.service.UserService;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;


import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;


@RestController("file")
@Slf4j
@RequestMapping("file")
public class FileController {

    @Resource
    private FilmService filmService;
    @Resource
    private UserService userService;

    @Resource
    private TokenService tokenService;

    @GetMapping("getRPB")
    public Map<String, Object> getRPB(@RequestHeader("Authorization") String token) {
        Map<String, String> parseToken = this.tokenService.parseToken(token);
        Map<String, Object> map = new HashMap<>();
        String tokenEmail = parseToken.get("email");
        String hxPassword = parseToken.get("hxPassword");
        List<User> userList = this.userService.getUser(tokenEmail);
        if (!userList.isEmpty() && Objects.equals(userList.get(0).getPassword(), hxPassword)) {
            List<Pf> pfs = this.filmService.selectP();
            map.put("msg", true);
            map.put("code", 200);
            map.put("data", pfs);
            return map;
        }
        map.put("msg", false);
        map.put("code", 500);
        map.put("hint", "无效token！");
        return map;
    }

    @GetMapping("getFile")
    public Map<String, Object> getFile(@RequestParam("k") int k,
                                       @RequestHeader("Authorization") String token) {
        Map<String, String> parseToken = this.tokenService.parseToken(token);
        Map<String, Object> map = new HashMap<>();
        String tokenEmail = parseToken.get("email");
        String hxPassword = parseToken.get("hxPassword");
        List<User> userList = this.userService.getUser(tokenEmail);
        if (!userList.isEmpty() && Objects.equals(userList.get(0).getPassword(), hxPassword)) {
            List<FilmCommon> film = this.filmService.getFilm(k, k + 39);
            map.put("msg", true);
            map.put("code", 200);
            map.put("data", film);
            return map;
        }
        map.put("msg", false);
        map.put("code", 500);
        map.put("hint", "无效token！");
        return map;
    }

    @GetMapping("getIndexFile")
    public Map<String, Object> getIndexFile(
            @RequestParam("k") int k,
            @RequestHeader("Authorization") String token) {
        Map<String, String> parseToken = this.tokenService.parseToken(token);
        Map<String, Object> map = new HashMap<>();
        String tokenEmail = parseToken.get("email");
        String hxPassword = parseToken.get("hxPassword");
        List<User> userList = this.userService.getUser(tokenEmail);
        if (!userList.isEmpty() && Objects.equals(userList.get(0).getPassword(), hxPassword)) {
            List<PfCommon> indexFilm = this.filmService.getIndexFilm(k, k + 39);
            map.put("msg", true);
            map.put("code", 200);
            map.put("data", indexFilm);
            return map;
        }
        map.put("msg", false);
        map.put("code", 500);
        map.put("hint", "无效token！");
        return map;
    }

    @PostMapping("setFile")
    public Map<String, Object> setFile(@RequestParam("name") String name,
                                       @RequestParam("director") String director,
                                       @RequestParam("zhuY") String zhuY,
                                       @RequestParam("date") String date,
                                       @RequestParam("time") String time,
                                       @RequestParam("lei") String lei,
                                       @RequestParam("file") MultipartFile file,
                                       @RequestHeader("Authorization") String token, HttpServletRequest request) {
        Map<String, String> parseToken = this.tokenService.parseToken(token);
        Map<String, Object> map = new HashMap<>();
        String tokenEmail = parseToken.get("email");
        String hxPassword = parseToken.get("hxPassword");
        List<User> userList = this.userService.getUser(tokenEmail);
        if (!userList.isEmpty() && Objects.equals(userList.get(0).getPassword(), hxPassword)) {
            Map<String, Object> stringObjectMap = this.filmService.setFilm(file);
            Object directory = stringObjectMap.get("directory");
            Object newFileName = stringObjectMap.get("newFileName");

            String url = "http://localhost:8080/images/" + directory + newFileName;

            boolean setFilm = this.filmService.setFilm(tokenEmail, name, director, zhuY, date, time, lei, "0", "0", url);
            if (setFilm) {
                map.put("msg", true);
                map.put("code", 200);
                map.put("hint", "添加成功！");
            } else {
                map.put("msg", false);
                map.put("code", 500);
                map.put("hint", "添加失败！");
            }
            return map;
        } else {
            map.put("msg", false);
            map.put("code", 500);
            map.put("hint", "无效token！");
        }
        return map;
    }

    @GetMapping("scFilm")
    public boolean scfilm(
            @RequestParam("name") String name,
            @RequestHeader("Authorization") String token
    ) {
        Map<String, String> parseToken = this.tokenService.parseToken(token);
        String tokenEmail = parseToken.get("email");
        String hxPassword = parseToken.get("hxPassword");
        List<User> userList = this.userService.getUser(tokenEmail);
        if (Objects.equals(tokenEmail, "admin") && !userList.isEmpty() && Objects.equals(userList.get(0).getPassword(), hxPassword)) {
            QueryWrapper<Film> filmQueryWrapper = new QueryWrapper<>();
            filmQueryWrapper.eq("name", name);
            this.filmService.remove(filmQueryWrapper);
            return true;
        }
        return false;
    }

    @PostMapping("upFile")
    public boolean upFile(@RequestParam("name") String name,
                          @RequestParam("nameNew") String nameNew,
                          @RequestParam("director") String director,
                          @RequestParam("zhuY") String zhuY,
                          @RequestParam("date") String date,
                          @RequestParam("time") String time,
                          @RequestParam("lei") String lei,
                          @RequestHeader("Authorization") String token) {
        Map<String, String> parseToken = this.tokenService.parseToken(token);
        String tokenEmail = parseToken.get("email");
        String hxPassword = parseToken.get("hxPassword");
        List<User> userList = this.userService.getUser(tokenEmail);
        if (!userList.isEmpty() && Objects.equals(userList.get(0).getPassword(), hxPassword) && Objects.equals(tokenEmail, "admin")) {
            UpdateWrapper<Film> filmUpdateWrapper = new UpdateWrapper<>();
            filmUpdateWrapper.eq("name", name)
                    .set("director", director)
                    .set("zhu", zhuY)
                    .set("date", date)
                    .set("time", time)
                    .set("name", nameNew)
                    .set("lei", lei);

            this.filmService.update(filmUpdateWrapper);
            return true;
        }
        return false;
    }
}
