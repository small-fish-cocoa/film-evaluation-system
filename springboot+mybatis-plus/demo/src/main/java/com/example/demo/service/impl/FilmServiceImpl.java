package com.example.demo.service.impl;

import cn.hutool.core.lang.UUID;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.common.FilmCommon;
import com.example.demo.common.PfCommon;
import com.example.demo.pojo.Film;
import com.example.demo.pojo.Pf;
import com.example.demo.service.FilmService;
import com.example.demo.mapper.FilmMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author 13465
 * @description 针对表【film】的数据库操作Service实现
 * @createDate 2024-06-17 22:48:29
 */
@Service
@Slf4j
public class FilmServiceImpl extends ServiceImpl<FilmMapper, Film> implements FilmService {

    /**
     * 时间格式化
     */
    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd/");

    /**
     * 图片保存路径，自动从yml文件中获取数据
     * 示例： E:/images/
     */
    @Value("${spring.file-save-path}")
    private String fileSavePath;

    public List<Pf> selectP() {
        return this.baseMapper.select_rm_pl();
    }

    @Override
    public Film selectFilm_name(String name) {
        QueryWrapper<Film> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("name", name);
        return this.getOne(queryWrapper);
    }
    @Override
    public List<PfCommon> getIndexFilm(int current, int size) {
        QueryWrapper<Film> userQueryWrapper = new QueryWrapper<>();
        userQueryWrapper.select("name", "zhang", "chai", "url");
        IPage<Film> page = new Page<>(current, size);
        IPage<Film> userPage = this.page(page, userQueryWrapper); // 调用 page 方法
        List<Film> FilmList = userPage.getRecords();
        List<PfCommon> pfCommonList = new ArrayList<>();
        for (Film film : FilmList) {
            PfCommon pfCommon = new PfCommon();
            pfCommon.setName(film.getName());
            pfCommon.setUrl(film.getUrl());
            double zhang = Integer.parseInt(film.getZhang());
            double chai = Integer.parseInt(film.getChai());
            double sum = zhang / (zhang + chai) * 5.0;
            String str = String.format("%.2f", sum);
            if ("NaN".equals(str)) {
                pfCommon.setSum("0");
            } else
                pfCommon.setSum(str);
            pfCommonList.add(pfCommon);

        }
        return pfCommonList;
    }


    ;

    @Override
    public List<FilmCommon> getFilm(int current, int size) {
        QueryWrapper<Film> userQueryWrapper = new QueryWrapper<>();
        userQueryWrapper.select("name", "director", "zhu", "date", "time", "lei", "zhang", "chai", "url");
        IPage<Film> page = new Page<>(current, size);
        IPage<Film> userPage = this.page(page, userQueryWrapper); // 调用 page 方法
        List<Film> FilmList = userPage.getRecords();
        List<FilmCommon> filmCommonList = new ArrayList<>();
        for (Film film : FilmList) {
            FilmCommon filmCommon = new FilmCommon();
            filmCommon.setName(film.getName());
            filmCommon.setDirector(film.getDirector());
            filmCommon.setZhu(film.getZhu());
            filmCommon.setDate(film.getDate());
            filmCommon.setTime(film.getTime());
            filmCommon.setLei(film.getLei());
            filmCommon.setZhang(film.getZhang());
            filmCommon.setChai(film.getChai());
            filmCommon.setImg_url(film.getUrl());
            filmCommonList.add(filmCommon);
        }
        return filmCommonList;
    }

    @Override
    public boolean setFilm(String email, String name, String director, String zhuY, String date, String time, String lei, String zhang, String chai, String imgUrl) {
        Film film = new Film();
        film.setEmail(email);
        film.setName(name);
        film.setDirector(director);
        film.setZhu(zhuY);
        film.setDate(date);
        film.setTime(time);
        film.setLei(lei);
        film.setZhang(zhang);
        film.setChai(chai);
        film.setUrl(imgUrl);
        return this.save(film);

    }

    @Override
    public Map<String, Object> setFilm(MultipartFile file) {
        Map<String, Object> map = new HashMap<>();

        String directory = simpleDateFormat.format(new Date());
        /**
         *  2.文件保存目录  E:/images/2020/03/15/
         *  如果目录不存在，则创建
         */
        File dir = new File(fileSavePath + directory);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        //3.给文件重新设置一个名字
        //后缀
        String suffix = Objects.requireNonNull(file.getOriginalFilename()).substring(file.getOriginalFilename().lastIndexOf("."));
        String newFileName = UUID.randomUUID().toString().replaceAll("-", "") + suffix;
        //4.创建这个新文件
        File newFile = new File(fileSavePath + directory + newFileName);
        //5.复制操作
        try {
            file.transferTo(newFile);
            //协议 :// ip地址 ：端口号 / 文件目录(/images/2020/03/15/xxx.jpg)
            map.put("msg", true);
            map.put("directory", directory);
            map.put("newFileName", newFileName);
        } catch (IOException e) {
            map.put("msg", false);
        }
        return map;
    }
}




