package com.example.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.pojo.Dysc;
import com.example.demo.service.DyscService;
import com.example.demo.mapper.DyscMapper;
import org.springframework.stereotype.Service;

/**
* @author 13465
* @description 针对表【dysc】的数据库操作Service实现
* @createDate 2024-06-24 15:30:54
*/
@Service
public class DyscServiceImpl extends ServiceImpl<DyscMapper, Dysc>
    implements DyscService{

}




