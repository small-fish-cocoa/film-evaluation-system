package com.example.demo.controller;

import com.example.demo.pojo.Film;
import com.example.demo.pojo.User;
import com.example.demo.service.FilmService;
import com.example.demo.service.TokenService;
import com.example.demo.service.UserService;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@RestController
@RequestMapping("cx")
public class CXController {

    @Resource
    private FilmService filmService;
    @Resource
    private UserService userService;

    @Resource
    private TokenService tokenService;

    @GetMapping("getOneFile")
    public Map<String, Object> getOneFile(@RequestParam("name") String name,
                                          @RequestHeader("Authorization") String token) {
        Map<String, String> parseToken = this.tokenService.parseToken(token);
        String email = parseToken.get("email");
        String hxPassword = parseToken.get("hxPassword");
        List<User> userList = this.userService.getUser(email);
        Map<String, Object> map = new HashMap<>();
        if (!userList.isEmpty() && Objects.equals(hxPassword, userList.get(0).getPassword())) {
            Film film = this.filmService.selectFilm_name(name);
            if (film != null) {
                map.put("film", film);
                map.put("msg",true);
                map.put("code",200);
            }else {
                map.put("msg",false);
                map.put("code",500);
                map.put("film","抱歉！找不到电影");
            }

            return map;
        }
        map.put("msg",false);
        map.put("code",500);
        return map;
    }

}
