package com.example.demo.pojo;


import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serial;
import java.io.Serializable;

import lombok.Data;

/**
 * @TableName user
 */
@TableName(value = "user")
@Data
public class User implements Serializable {
    private String email;

    private String password;

    private String gender;

    private String user;

    private String phone;

    private String district;
    private String img;

    @Serial
    private static final long serialVersionUID = 1L;
}