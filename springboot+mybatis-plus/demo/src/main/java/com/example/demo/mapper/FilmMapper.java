package com.example.demo.mapper;

import com.example.demo.pojo.Film;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.pojo.Index;
import com.example.demo.pojo.Pf;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.context.annotation.Bean;

import java.util.List;
import java.util.Map;

/**
 * @author 13465
 * @description 针对表【film】的数据库操作Mapper
 * @createDate 2024-06-17 22:48:29
 * @Entity com.example.demo.pojo.Film
 */
@Mapper
public interface FilmMapper extends BaseMapper<Film> {
    List<Pf> select_rm_pl();
    List<Index> selectIndexFile();
}




