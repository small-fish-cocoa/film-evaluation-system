package com.example.demo.mapper;

import com.example.demo.pojo.Pn;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 13465
* @description 针对表【pn】的数据库操作Mapper
* @createDate 2024-06-24 15:30:36
* @Entity com.example.demo.pojo.Pn
*/
public interface PnMapper extends BaseMapper<Pn> {

}




