package com.example.demo.common;

import lombok.Data;

@Data
public class FilmCommon {
    private String name;
    private String director;
    private String zhu;
    private String date;
    private String time;
    private String lei;
    private String zhang;
    private String chai;
    private String img_url;

    public FilmCommon JSONFilm(String name, String director, String zhu, String date, String time, String lei, String zhang, String chai) {
        FilmCommon filmCommon = new FilmCommon();
        filmCommon.setName(name);
        filmCommon.setDirector(director);
        filmCommon.setZhu(zhu);
        filmCommon.setDate(date);
        filmCommon.setTime(time);
        filmCommon.setLei(lei);
        filmCommon.setZhang(zhang);
        filmCommon.setChai(chai);
        return filmCommon;
    }

}
