package com.example.demo.service;

import com.example.demo.pojo.Dysc;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 13465
* @description 针对表【dysc】的数据库操作Service
* @createDate 2024-06-24 15:30:54
*/
public interface DyscService extends IService<Dysc> {

}
