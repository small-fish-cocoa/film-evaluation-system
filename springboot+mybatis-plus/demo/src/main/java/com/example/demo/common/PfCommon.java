package com.example.demo.common;

import lombok.Data;

@Data
public class PfCommon {
    private String name;
    private String url;
    private String sum;

    public PfCommon JSONPf(String name, String url, String sum) {
        PfCommon pfCommon = new PfCommon();
        pfCommon.setName(name);
        pfCommon.setUrl(url);
        pfCommon.setSum(sum);
        return pfCommon;
    }
}
