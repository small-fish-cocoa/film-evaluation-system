package com.example.demo.service;

import com.example.demo.pojo.Pn;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
* @author 13465
* @description 针对表【pn】的数据库操作Service
* @createDate 2024-06-24 15:30:36
*/
public interface PnService extends IService<Pn> {

    List<Map<String, Object>> selectAll(String name, int current, int size);

    void setPn(String film, String email, String text, String user, String url);
}
