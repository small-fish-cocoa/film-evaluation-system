package com.example.demo.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

import lombok.Data;

/**
 * @TableName dysc
 */
@TableName(value = "dysc")
@Data
public class Dysc implements Serializable {
    private Integer id;
    private String email;

    private String name;

    private String url;
    private String zhan;
    private static final long serialVersionUID = 1L;
}