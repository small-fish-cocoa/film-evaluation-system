package com.example.demo.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * @TableName jb
 */
@TableName(value ="jb")
@Data
public class Jb implements Serializable {
    private Integer id;

    private String emaila;

    private String emailn;

    private String data;

    private static final long serialVersionUID = 1L;
}