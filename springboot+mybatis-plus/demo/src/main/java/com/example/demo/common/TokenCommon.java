package com.example.demo.common;

import lombok.Data;

@Data
public class TokenCommon {
    private String email;
    private boolean msg;
    private int code;
    private String token;

    public TokenCommon tokenJSON(String email, boolean msg, int code, String token) {
        TokenCommon tokenCommon = new TokenCommon();
        tokenCommon.setEmail(email);
        tokenCommon.setMsg(msg);
        tokenCommon.setCode(code);
        tokenCommon.setToken(token);
        return tokenCommon;
    }
}
