package com.example.demo.mapper;

import com.example.demo.pojo.Jb;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 13465
* @description 针对表【jb】的数据库操作Mapper
* @createDate 2024-06-25 22:05:49
* @Entity com.example.demo.pojo.Jb
*/
public interface JbMapper extends BaseMapper<Jb> {

}




