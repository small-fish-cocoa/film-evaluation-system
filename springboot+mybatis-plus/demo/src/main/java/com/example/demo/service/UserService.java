package com.example.demo.service;

import com.example.demo.pojo.User;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author 13465
 * @description 针对表【user】的数据库操作Service
 * @createDate 2024-06-17 19:07:37
 */
public interface UserService extends IService<User> {
    List<User> getUser(String email);

    String setUser(String email);

    String updateUserPassword(String email, String newPassword);

    void updateUser(String email, String user, String gender, String phone, String district);

    List<User> getAllUser(int current, int size);
}
