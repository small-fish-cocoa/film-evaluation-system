package com.example.demo.service;

import com.example.demo.common.FilmCommon;
import com.example.demo.common.PfCommon;
import com.example.demo.pojo.Film;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.demo.pojo.Index;
import com.example.demo.pojo.Pf;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * @author 13465
 * @description 针对表【film】的数据库操作Service
 * @createDate 2024-06-17 22:48:29
 */
public interface FilmService extends IService<Film> {

    Film selectFilm_name(String name);

    List<PfCommon> getIndexFilm(int current, int size);

    List<FilmCommon> getFilm(int current, int size);

    boolean setFilm(String email, String name, String director, String zhuY, String date, String time, String lei, String zhang, String chai, String imgUrl);

    Map<String, Object> setFilm(MultipartFile file);

    List<Pf> selectP();

}
