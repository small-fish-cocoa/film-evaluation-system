package com.example.demo.mapper;

import com.example.demo.pojo.Dysc;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 13465
* @description 针对表【dysc】的数据库操作Mapper
* @createDate 2024-06-24 15:30:54
* @Entity com.example.demo.pojo.Dysc
*/
public interface DyscMapper extends BaseMapper<Dysc> {

}




