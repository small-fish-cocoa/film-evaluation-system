package com.example.demo.service;



public interface RedisService {


    void set(String key, String value);

    Long getTime(String key);

    void setExpire(String key, String value, long s);

    String get(String key);

    void delete(String key);

    Boolean expire(String key, int s);


    boolean verifyNumber(String key, int number);

}
