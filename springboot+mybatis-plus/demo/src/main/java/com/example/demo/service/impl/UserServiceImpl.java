package com.example.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.pojo.User;
import com.example.demo.service.UserService;
import com.example.demo.mapper.UserMapper;
import com.example.demo.util.MD5Util;
import com.example.demo.util.RandomUtil;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 13465
 * @description 针对表【user】的数据库操作Service实现
 * @createDate 2024-06-17 19:07:37
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User>
        implements UserService {


    @Override
    public List<User> getUser(String email) {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("email", email);
        return this.list(queryWrapper);
    }

    @Override
    public String setUser(String email) {
        User user = new User();
        user.setEmail(email);
        String r = RandomUtil.random1(10);
        String md5 = MD5Util.md5(r);
        user.setPassword(md5);
        user.setImg("http://localhost:8080/images/2024/06/20/f1cd279b0ce745bbbd8d3837028b26b4.png");
        user.setUser("欢迎新用户");
        user.setGender("男");
        this.save(user);
        return md5;
    }

    @Override
    public String updateUserPassword(String email, String newPassword) {
        UpdateWrapper<User> updateWrapper = new UpdateWrapper<>();
        String md5 = MD5Util.md5(newPassword);
        updateWrapper.eq("email", email).set("password", md5);
        this.update(updateWrapper);
        return md5;

    }

    @Override
    public void updateUser(String email, String user, String gender, String phone, String district) {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("email", email);
        User newUser = new User();
        newUser.setUser(user);
        newUser.setGender(gender);
        newUser.setPhone(phone);
        newUser.setDistrict(district);
        this.update(newUser, queryWrapper);


    }

    @Override
    public List<User> getAllUser(int current, int size) {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        IPage<User> page = new Page<>(current, size);
        IPage<User> paged = this.page(page);
        List<User> records = paged.getRecords();
        List<User> pfCommonList = new ArrayList<>();
        for (User film : records) {
            User user = new User();
            String email = film.getEmail();
            user.setEmail(email);
            user.setGender(film.getGender());
            user.setPhone(film.getPhone());
            user.setDistrict(film.getDistrict());
            user.setUser(film.getUser());
            user.setImg(film.getImg());
            if (email.equals("admin")) {
                user.setPassword("管理员用户");
            } else {
                user.setPassword("普通用户");
            }
            pfCommonList.add(user);
        }
        return pfCommonList;
    }

}




