package com.example.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.pojo.Jb;
import com.example.demo.service.JbService;
import com.example.demo.mapper.JbMapper;
import org.springframework.stereotype.Service;

/**
* @author 13465
* @description 针对表【jb】的数据库操作Service实现
* @createDate 2024-06-25 22:05:49
*/
@Service
public class JbServiceImpl extends ServiceImpl<JbMapper, Jb>
    implements JbService{

}




