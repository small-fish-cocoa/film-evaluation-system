package com.example.demo.util;

public class EmailTextUtil {
    public static String emailText(String num, int k) {
        if (k == 1) {
            return "验证码:" + num + ",您正在使用验证码登录diary账号，请勿将验证码告诉他人，否则容易造成账号安全问题！验证码十分钟内有效。";
        }
        return "验证码:" + num + ",您正在使用验证码修改diary账号密码，请勿将验证码告诉他人，否则容易造成账号安全问题！验证码十分钟内有效。";

    }
}
