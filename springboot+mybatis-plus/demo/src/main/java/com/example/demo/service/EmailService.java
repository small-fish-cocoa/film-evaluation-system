package com.example.demo.service;


/**
 * 发送邮件：【模板邮件、附件邮件、普通邮件】的简单实现
 *
 * @author xiao
 * @since 2023-12-24
 */


public interface EmailService {
    boolean sendMailForTemplateEngine(String email, int k);
}
