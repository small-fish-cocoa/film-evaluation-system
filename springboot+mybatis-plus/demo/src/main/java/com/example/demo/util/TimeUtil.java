package com.example.demo.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeUtil {

    public static String getCurrentTime() {
        // 创建一个SimpleDateFormat对象，用于格式化时间
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        // 获取系统当前时间
        Date date = new Date();

        // 使用SimpleDateFormat对象将时间格式化为指定格式
        return sdf.format(date);
    }
}
