package com.example.demo.service;

import com.example.demo.pojo.Jb;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 13465
* @description 针对表【jb】的数据库操作Service
* @createDate 2024-06-25 22:05:49
*/
public interface JbService extends IService<Jb> {

}
