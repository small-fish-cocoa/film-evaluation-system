package com.example.demo.util;

import java.util.Random;

public class RandomUtil {
    public static String random1(int n) {
        StringBuilder stringBuilder = new StringBuilder();
        // 使用 Random随机生成数
        Random random = new Random();
        while (stringBuilder.length() < n) {
            // 定义6位长度，每次随机生成一位数字，存在stringBuilder中
            stringBuilder.append(random.nextInt(10));
        }
        return stringBuilder.toString();
    }
}
