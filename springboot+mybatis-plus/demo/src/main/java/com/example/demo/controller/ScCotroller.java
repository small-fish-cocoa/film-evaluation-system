package com.example.demo.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.demo.pojo.Dysc;
import com.example.demo.pojo.User;
import com.example.demo.service.DyscService;
import com.example.demo.service.TokenService;
import com.example.demo.service.UserService;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@RestController
@RequestMapping("sc")
public class ScCotroller {

    @Resource
    private UserService userService;
    @Resource
    private TokenService tokenService;

    @Resource
    private DyscService dyscService;

    @PostMapping("dys")
    public Map<String, Object> setDysc(@RequestParam("name") String name,
                                       @RequestParam("url") String url,
                                       @RequestHeader("Authorization") String token) {
        Map<String, Object> map = new HashMap<>();
        Map<String, String> parseToken = this.tokenService.parseToken(token);
        String email = parseToken.get("email");
        String hxPassword = parseToken.get("hxPassword");
        List<User> userList = this.userService.getUser(email);
        if (!userList.isEmpty() && Objects.equals(hxPassword, userList.get(0).getPassword())) {
            Dysc dysc = new Dysc();
            dysc.setEmail(email);
            dysc.setUrl(url);
            dysc.setName(name);
            this.dyscService.save(dysc);
            map.put("msg", true);
            map.put("code", 200);
            return map;
        }
        map.put("msg", false);
        map.put("code", 400);
        return map;
    }

    @GetMapping("removeSc")
    public Map<String, Object> upDysc(@RequestParam("name") String name,
                                      @RequestHeader("Authorization") String token) {
        Map<String, String> parseToken = this.tokenService.parseToken(token);
        String email = parseToken.get("email");
        String hxPassword = parseToken.get("hxPassword");
        Map<String, Object> map = new HashMap<>();
        List<User> userList = this.userService.getUser(email);
        if (!userList.isEmpty() && Objects.equals(hxPassword, userList.get(0).getPassword())) {
            QueryWrapper<Dysc> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("email", email).eq("name", name);
            this.dyscService.remove(queryWrapper);
            map.put("msg", true);
            map.put("code", 200);
            return map;
        }
        map.put("msg", false);
        map.put("code", 400);
        return map;
    }

    @GetMapping("getDysc")
    public Map<String, Object> getDysc(@RequestParam("filme") String filme,
                                       @RequestHeader("Authorization") String token) {
        Map<String, String> parseToken = this.tokenService.parseToken(token);
        String email = parseToken.get("email");
        String hxPassword = parseToken.get("hxPassword");
        Map<String, Object> map = new HashMap<>();
        List<User> userList = this.userService.getUser(email);
        if (!userList.isEmpty() && Objects.equals(hxPassword, userList.get(0).getPassword())) {
            QueryWrapper<Dysc> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("email", email).eq("name", filme);
            Dysc one = this.dyscService.getOne(queryWrapper, false);
            map.put("msg", true);
            map.put("code", 200);
            map.put("data", one);
            return map;
        }
        map.put("msg", false);
        map.put("code", 400);
        return map;
    }
}
