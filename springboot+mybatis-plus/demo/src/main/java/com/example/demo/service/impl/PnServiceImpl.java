package com.example.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.pojo.Pn;
import com.example.demo.service.PnService;
import com.example.demo.mapper.PnMapper;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author 13465
 * @description 针对表【pn】的数据库操作Service实现
 * @createDate 2024-06-24 15:30:36
 */
@Service
public class PnServiceImpl extends ServiceImpl<PnMapper, Pn>
        implements PnService {

    @Override
    public List<Map<String, Object>> selectAll(String name, int current, int size) {

        QueryWrapper<Pn> userQueryWrapper = new QueryWrapper<>();
        userQueryWrapper.eq("film", name);
        userQueryWrapper.select("time", "email", "textd", "user", "url", "zhan", "chai", "id");
        IPage<Pn> page = new Page<>(current, size);
        IPage<Pn> PnPage = this.page(page, userQueryWrapper); // 调用 page 方法
        List<Pn> PnList = PnPage.getRecords();
        List<Map<String, Object>> pfCommonList = new ArrayList<>();
        for (Pn pn : PnList) {
            Map<String, Object> map = new HashMap<>();
            map.put("time", pn.getTime());
            map.put("user", pn.getUser());
            map.put("email", pn.getEmail());
            map.put("textd", pn.getTextd());
            map.put("zhan", pn.getZhan());
            map.put("chai", pn.getChai());
            map.put("url", pn.getUrl());
            map.put("id", pn.getId());
            pfCommonList.add(map);
        }
        return pfCommonList;
    }

    @Override
    public void setPn(String film, String email, String text, String user, String url) {
        Pn pn = new Pn();
        pn.setFilm(film);
        pn.setEmail(email);
        pn.setTextd(text);
        pn.setUser(user);
        pn.setJb("0");
        pn.setUrl(url);
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        pn.setTime(formatter.format(date));
        pn.setZhan(0);
        pn.setChai(0);
        this.save(pn);
    }
}




