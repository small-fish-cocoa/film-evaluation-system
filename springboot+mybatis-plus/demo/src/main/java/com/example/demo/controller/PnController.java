package com.example.demo.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.example.demo.pojo.Pn;
import com.example.demo.pojo.User;
import com.example.demo.service.PnService;
import com.example.demo.service.TokenService;
import com.example.demo.service.UserService;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@RestController("pn")
@RequestMapping("pn")
public class PnController {
    @Resource
    public UserService userService;
    @Resource
    public TokenService tokenService;

    @Resource
    public PnService pnService;

    @GetMapping("getpn")
    public Map<String, Object> getpn(@RequestParam("name") String name, @RequestParam("k") int k, @RequestHeader("Authorization") String token) {

        Map<String, String> parseToken = this.tokenService.parseToken(token);
        String email = parseToken.get("email");
        String hxPassword = parseToken.get("hxPassword");
        List<User> userList = this.userService.getUser(email);
        Map<String, Object> map = new HashMap<>();
        if (!userList.isEmpty() && Objects.equals(hxPassword, userList.get(0).getPassword())) {
            List<Map<String, Object>> maps = this.pnService.selectAll(name, k, k + 19);
            map.put("data", maps);
            map.put("code", 200);
            map.put("msg", true);
            return map;
        }

        map.put("msg", false);
        map.put("code", 500);
        return map;

    }

    @PostMapping("setpn")
    public Map<String, Object> setPn(@RequestParam("data") String data, @RequestParam("file") String file, @RequestHeader("Authorization") String token) {

        Map<String, String> parseToken = this.tokenService.parseToken(token);
        String email = parseToken.get("email");
        String hxPassword = parseToken.get("hxPassword");
        List<User> userList = this.userService.getUser(email);
        Map<String, Object> map = new HashMap<>();
        if (!userList.isEmpty() && Objects.equals(hxPassword, userList.get(0).getPassword())) {
            this.pnService.setPn(file, email, data, userList.get(0).getUser(), userList.get(0).getImg());
            map.put("msg", true);
            map.put("code", 200);
            return map;
        }

        map.put("msg", false);
        map.put("code", 500);
        return map;
    }

    @GetMapping("jbc")
    public Map<String, Object> jb(@RequestParam("email") String email,
                                  @RequestParam("file") String fime,
                                  @RequestParam("id") String id,
                                  @RequestHeader("Authorization") String token) {
        Map<String, String> parseToken = this.tokenService.parseToken(token);
        String hxPassword = parseToken.get("hxPassword");
        String email1 = parseToken.get("email");
        Map<String, Object> map = new HashMap<>();
        List<User> userList = this.userService.getUser(email1);
        if (!userList.isEmpty() && Objects.equals(hxPassword, userList.get(0).getPassword())) {
            UpdateWrapper<Pn> queryWrapper = new UpdateWrapper<>();
            queryWrapper.eq("email", email).eq("film", fime).eq("textd", id).set("jb", "1");
            this.pnService.update(queryWrapper);
            map.put("msg", true);
            map.put("code", 200);
            return map;
        }
        map.put("token", "无效token");
        map.put("msg", false);
        map.put("code", 500);
        return map;
    }


    @GetMapping("getReport")
    public Map<String, Object> getReport(@RequestHeader("Authorization") String token) {
        Map<String, String> parseToken = this.tokenService.parseToken(token);
        String email = parseToken.get("email");
        String hxPassword = parseToken.get("hxPassword");
        List<User> userList = this.userService.getUser(email);
        Map<String, Object> map = new HashMap<>();
        if (Objects.equals("admin", email) && !userList.isEmpty() && Objects.equals(hxPassword, userList.get(0).getPassword())) {
            QueryWrapper<Pn> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("jb", "1");
            List<Pn> list = this.pnService.list(queryWrapper);
            map.put("data", list);
            map.put("code", 200);
            map.put("msg", true);
            return map;
        }
        map.put("msg", false);
        map.put("code", 500);
        return map;
    }

    @GetMapping("upReport")
    public boolean upReport(
            @RequestParam("id") int id,
            @RequestHeader("Authorization") String token) {
        Map<String, String> parseToken = this.tokenService.parseToken(token);
        String email = parseToken.get("email");
        String hxPassword = parseToken.get("hxPassword");
        List<User> userList = this.userService.getUser(email);
        if (Objects.equals("admin", email) && !userList.isEmpty() && Objects.equals(hxPassword, userList.get(0).getPassword())) {

            this.pnService.removeById(id);
            return true;
        }

        return false;
    }
}
