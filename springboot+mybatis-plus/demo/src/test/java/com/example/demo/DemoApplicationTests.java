package com.example.demo;

import com.baomidou.mybatisplus.core.toolkit.ClassUtils;
import com.example.demo.mapper.FilmMapper;
import com.example.demo.pojo.Film;
import com.example.demo.pojo.Pf;
import com.example.demo.service.FilmService;
import com.example.demo.service.PnService;
import com.example.demo.service.UserService;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import static org.springframework.boot.web.servlet.server.Session.SessionTrackingMode.URL;

@SpringBootTest
class DemoApplicationTests {

    @Resource
    private UserService userService;

    @Resource
    private FilmService filmService;
    @Resource
    private FilmMapper filmMapper;

    @Resource
    private PnService pnService;

    @Test
    void contextLoads() {
        List<Map<String, Object>> maps = this.pnService.selectAll("长津湖", 1, 10);
        System.out.println(maps);
        this.pnService.setPn("1", "1", "2", "2", "4");
    }

    @PostMapping("/single")
    public String singleImage(@RequestParam("file") MultipartFile file, HttpServletRequest request) throws FileNotFoundException {  //参数名需与前端文件标签名一样
        //获取项目classes/static的地址
        String path = ClassUtils.getDefaultClassLoader().getResource("static").getPath();
        String fileName = file.getOriginalFilename();  //获取文件名
        //图片访问URI(即除了协议、地址和端口号的URL)
        String url_path = "image" + File.separator + fileName;
        System.out.printf("图片访问uri：" + url_path);
        String savePath = path + File.separator + url_path;  //图片保存路径
        System.out.printf("图片保存地址：" + savePath);
        File saveFile = new File(savePath);
        if (!saveFile.exists()) {
            saveFile.mkdirs();
        }
        try {
            file.transferTo(saveFile);  //将临时存储的文件移动到真实存储路径下
        } catch (IOException e) {
            e.printStackTrace();
        }
        //返回图片访问地址
        System.out.printf("访问URL：" + URL + url_path);
        return "121";
    }
}
